package com.cts.cde.io.onboarding.content;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/contents")
@CrossOrigin
public class ContentController {
    @Autowired
    private ContentService contentService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<ContentFilter>> getAllContent(@RequestParam("name") String name) {
        List<ContentFilter> contents = contentService.getContentByRoleId(name);
        return new ResponseEntity<List<ContentFilter>>(contents, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Content>  getContent(@PathVariable Integer id) {
        Content content = contentService.getContent(id);
        return new ResponseEntity<Content>(content, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Content> createContent(@RequestBody Content content) {
        Content contentData = contentService.createContent(content);
        return new ResponseEntity<Content>(contentData, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Content> updateContent(@RequestBody Content content, @PathVariable Integer id) {
        Content contentData = contentService.updateContent(content);
        return new ResponseEntity<Content>(contentData, HttpStatus.OK);
    }

    //Changed the method to handle different JSON request body.
//    @RequestMapping(method = RequestMethod.PUT)
//    public ResponseEntity<List<Content>> rearrangeContent(@RequestBody List<Content> contents) {
//        List<Content> contentDataList=new ArrayList<Content>();
//        for (Content content : contents) {
//            Content contentData = contentService.updateContent(content);
//            contentDataList.add(contentData);
//        }
//        return new ResponseEntity<List<Content>>(contentDataList, HttpStatus.OK);
//    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<String> rearrangeContent(@RequestBody List<Content> contents) {
        contentService.updateContentOrder(contents);
        return new ResponseEntity<String>(HttpStatus.OK);
    }
    @RequestMapping(method=RequestMethod.GET,value = "/substage/{subStageId}")
    public ResponseEntity<Integer> countContentBysubStageId(@PathVariable Integer subStageId){
        Integer count=contentService.countContentBySubStageId(subStageId);
        return new ResponseEntity<Integer>(count,HttpStatus.OK);
    };


    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<String> deleteContent(@PathVariable Integer id) {

        contentService.deleteContent(id);
        return new ResponseEntity<String>(HttpStatus.OK);
    }

}
