package com.cts.cde.io.onboarding.contentstatus;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/content-status")
public class ContentStatusController {

    @Autowired
    ContentStatusService service;


    @RequestMapping(method = RequestMethod.GET, value ="/{userId}")
    public ResponseEntity<List<ContentStatusFilter>> getContentStatusByUserId(@PathVariable Integer userId) {
        List<ContentStatusFilter> contentStatusList = service.getContentStatusByUserId(userId);
        return new ResponseEntity<List<ContentStatusFilter>>(contentStatusList, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createOrUpdateContentStatus(@RequestBody ContentStatus contentStatus) {

        ContentStatus contentStatusData = service.createUserContentStatus(contentStatus);
        HttpStatus httpStatus = HttpStatus.CREATED;
        if (contentStatusData == null) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity(httpStatus);
    }
}

