package com.cts.cde.io.onboarding.content;

import com.cts.cde.io.onboarding.contentstatus.ContentStatus;
import com.cts.cde.io.onboarding.role.Role;
import com.cts.cde.io.onboarding.sub_stage.SubStage;
import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.cts.cde.io.onboarding.role.Role;
import com.cts.cde.io.onboarding.sub_stage.SubStage;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "content")
public class Content{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "HEADER", length=1000)
    private String header;

    @Column(name = "DESCRIPTION", length=2500)
    private String description;
// added by Jesudoss for content order
    @Column(name= "DISPLAYORDER")
    private int displayOrder;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sub_stage_id", nullable = false)
    //Json back reference commented because sub-stage id not displaying in get content api
    //@JsonBackReference
    private SubStage subStage;

// Added Cascade type as MERGE for creating Document while Editing Content
    @OneToMany(
            mappedBy = "content",
            cascade = {CascadeType.MERGE,
            CascadeType.PERSIST},
            fetch = FetchType.LAZY,
            orphanRemoval = true)
    @JsonManagedReference

    private List<Document> documents = new ArrayList<>();


    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "content_role",
            joinColumns = @JoinColumn(name = "content_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles;

    @Column(name = "FOOTER", length=1000)
    private String footer;

    @Column(name = "CREATED_ON")
    @JsonFormat(pattern = "YYYY-MM-dd")
    private Date createdOn;


    public Content() {
    }
    public Content(int id, String name, SubStage subStage, Set<Role> roles,
                   String title, String header, String description, String footer,
                   Date createdOn, List<Document> documents,int displayOrder) {
        this.id = id;
        this.name = name;
        this.subStage = subStage;
        this.roles = roles;
        this.title = title;
        this.header = header;
        this.description = description;
        this.footer = footer;
        this.createdOn = createdOn;
        this.documents = documents;
        this.displayOrder=displayOrder;

    }



    public Content(int id, String name, SubStage subStage,int displayOrder) {
        this.id = id;
        this.name = name;
        this.subStage = subStage;
        this.displayOrder=displayOrder;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public List<Document> getDocuments() {
        return documents;
    }


    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public SubStage getSubStage() {
        return subStage;
    }

    public void setSubStage(SubStage subStage) {
        this.subStage = subStage;
    }

    public int getDisplayOrder() { return displayOrder; }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Override
    public String toString() {
        String result = String.format(
                "Category[id=%d, name='%s']%n",
                id, name);
        if (documents != null) {
            for (Document document : documents) {
                result += String.format(
                        "document[id=%d, URL='%s', type= '%s']%n",
                        document.getId(), document.getURL(), document.getType());
            }
        }

        return result;
    }

}
