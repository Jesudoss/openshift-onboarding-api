package com.cts.cde.io.onboarding.feedback;

import java.util.List;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cts.cde.io.onboarding.usermanagement.User;


@RestController
@RequestMapping("/feedback")
@CrossOrigin
public class FeedBackController {
	
	  @Autowired
	  private FeedBackService feedbackService;

	
	@RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<FeedBack>> getAllFeedbacks() {
        List<FeedBack> userFeedbackData = feedbackService.getAllistFeedbacks();
        return new ResponseEntity<List<FeedBack>>(userFeedbackData, HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.GET, value ="/all")
    public ResponseEntity<List<FeedBack>> getAllFeedbackById() {
    	List<FeedBack> userFeedbackInfo = feedbackService.getAllFeedbackList();
        return new ResponseEntity<List<FeedBack>>(userFeedbackInfo, HttpStatus.OK);
    }
    @RequestMapping(method = RequestMethod.GET, value ="/{id}")
    public ResponseEntity<Object> getFeedbackById(@PathVariable Integer id) throws JSONException {
    	Object userFeedbackInfo = feedbackService.getFeedbackById(id);
    	System.out.println("****************In Controller************************"+userFeedbackInfo.toString());
        return new ResponseEntity<Object>(userFeedbackInfo, HttpStatus.OK);
    }
    @RequestMapping(method = RequestMethod.GET, value ="/all/{id}")
    public ResponseEntity<List<FeedBack>> getFeedbackAllById(@PathVariable Integer id) throws JSONException {
    	List<FeedBack> userFeedbackInfo = feedbackService.getFeedbackAllById(id);
        return new ResponseEntity<List<FeedBack>>(userFeedbackInfo, HttpStatus.OK);
    }
    
    
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> createUserFeedback(@RequestBody FeedBack feedback) {
    	feedbackService.createUserFeedback(feedback);
        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<User> updateFeedback(@RequestBody FeedBack feedBack, @PathVariable Integer id) {
		feedbackService.updateFeedback(feedBack, id);
        return new ResponseEntity<User>( HttpStatus.OK);
    }
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<FeedBack> deleteFeedback(@PathVariable Integer id) {
		feedbackService.deleteFeedback(id);
        return new ResponseEntity<FeedBack>( HttpStatus.OK);
    }
}
