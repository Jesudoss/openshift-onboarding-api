package com.cts.cde.io.onboarding.contentstatus;



import com.cts.cde.io.onboarding.usermanagement.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContentStatusService {

    @Autowired
    public ContentStatusRepository contentStatusRepository;

    @Autowired
    public UserRepository userRepository;

    private ArrayList<ContentStatus> contentStatuses = new ArrayList<>();

    public List<ContentStatusFilter> getContentStatusByUserId(int userId) {
        List<ContentStatusFilter> contentStatuses = contentStatusRepository.findByUser(userId);
        return contentStatuses;
    }

    public ContentStatus createUserContentStatus(ContentStatus contentStatus) {
        //expecting content status object from controller
        ContentStatus contentStatusData = contentStatusRepository.save(contentStatus);
        return contentStatusData;
    }
}
