package com.cts.cde.io.onboarding.sub_stage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Service
public class SubStageService {

    @Autowired
    private SubStageRepository subStageRepository;

    public List<SubStageFilter> getSubStageList() {
        List<SubStageFilter> subStages = new ArrayList<>();
        subStageRepository.findAllWithFilter().forEach(subStages::add);
        return subStages;
    }

    public SubStage getSubStage(Integer id) {
        return subStageRepository.findOne(id);
    }

    public SubStage createSubStage(SubStage subStage) {
        return subStageRepository.save(subStage);
    }

    @Transactional
    public void updateSubStage(SubStage subStage, Integer id) {
        System.out.println("@@@@Updating substage");
         subStageRepository.updateSubStage(subStage.getName(),subStage.getId(),subStage.getStage());
    }


    public void deleteSubStage(Integer id) {
        subStageRepository.delete(id);
    }

}
