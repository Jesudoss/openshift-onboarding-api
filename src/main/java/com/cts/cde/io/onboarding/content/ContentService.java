package com.cts.cde.io.onboarding.content;

import com.cts.cde.io.onboarding.usermanagement.User;
import com.cts.cde.io.onboarding.usermanagement.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class ContentService {

    @Autowired
    private ContentRepository contentRepository;

    @Autowired
    private UserRepository userRepository;

    public List<Content> getContentList() {
        List<Content> contents = new ArrayList<>();
        contentRepository.findAll().forEach(contents::add);
        return contents;
    }

    public Content getContent(Integer id) {
        return contentRepository.findOne(id);
    }

    public List<ContentFilter> getContentByRoleId(String name) {

        User user=userRepository.findByUserName(name);
        if (user.getAdmin()) {
            return contentRepository.findAllForAdmin();
        }
        return contentRepository.findByRoleId(name);
    }

    public Content createContent(Content content) {

        //Code to assign the content display order

//        int subStageId=content.getSubStage().getId();
//        int count=contentRepository.countContent(subStageId);
//        if(count>0) {
//            List<Content> contents = getContentList();
//            List<Content> listContent = new ArrayList<>();
//            for (Content contentFilter : contents) {
//                if (contentFilter.getSubStage().getId() == subStageId) {
//                    listContent.add(contentFilter);
//                }
//
//            }
//            int displayOrder = listContent.size();
//            content.setDisplayOrder(displayOrder + 1);
//        }
//        else
//        {
//            content.setDisplayOrder(1);
//        }
        List<Document> documents = content.getDocuments();
        for (Document document : documents) {
            document.setContent(content);
        }
        content.setDocuments(documents);


        content = contentRepository.save(content);
        content.setDocuments(content.getDocuments());
        return content;
    }

    public void updateContentOrder(List<Content> contents){
        for (Content content:contents) {
            contentRepository.updateContentOrder(content.getId(),content.getDisplayOrder());
        }
    }

    public Content updateContent(Content content) {
        
        return contentRepository.save(content);
    }
     //For loop is implemented for displayorder update. Functionality should be updated with
    // sql query
    public void deleteContent(Integer id) {
       int substageID=getContent(id).getSubStage().getId();
       int displayOrder=getContent(id).getDisplayOrder();
       int count=contentRepository.countContent(substageID);
       if(count>0)
       {
       List<Content> contents=getContentList();
       for(Content content:contents)
       {
           if(content.getSubStage().getId()==substageID && content.getDisplayOrder()>displayOrder)
           {
               content.setDisplayOrder((content.getDisplayOrder())-1);
               updateContent(content);
           }
       }
       }
        contentRepository.deleteAllUserContentStatus(id);
        contentRepository.delete(id);

    }

    public int countContentBySubStageId(Integer subStageId)
    {
        return contentRepository.countContent(subStageId);
    }

}
