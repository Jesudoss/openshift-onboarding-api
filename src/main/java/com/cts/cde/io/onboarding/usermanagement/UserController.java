package com.cts.cde.io.onboarding.usermanagement;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cts.cde.io.onboarding.role.ErrorMessage;
import com.google.gson.Gson;


@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<User>> getAllUser() {
        List<User> userData = userService.getUserList();
        return new ResponseEntity<List<User>>(userData, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userName}/")
    public ResponseEntity<User> getUser(@PathVariable String userName) {
        User userData = userService.getUserByUserName(userName);
        return new ResponseEntity<User>(userData, HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/userName/{userName}")
    public ResponseEntity<UserRole> getUserRole(@PathVariable String userName) {
        UserRole userData = userService.getUserByUserNamewithRole(userName);
        return new ResponseEntity<UserRole>(userData, HttpStatus.OK);
    }


    @PostMapping("/sign-up")
    public ResponseEntity<ErrorMessage> createUser(@RequestBody User user) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Expose-Headers", "Authorization");
        ErrorMessage status = userService.createUser(user);
        return new ResponseEntity<ErrorMessage>(status, headers, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/authenticateUser")
    public ResponseEntity<String> authenticateUser(@RequestHeader("user_name") String user_name,
                                                   @RequestHeader("password") String password) {

        ResponseEntity<String> response = null;
        User user = userService.authenticateUser(user_name, password);
        Map<String, Object> message = new HashMap<String, Object>();
        if (user != null) {
            message.put("message", "User Authenticated Successfully.");
            message.put("firstName", user.getFirstName());
            message.put("userName", user.getUserName());
            message.put("Status", "Sucess");
            response = new ResponseEntity<String>(new Gson().toJson(message), HttpStatus.OK);
        } else {
            message.put("message", "Invalid User Credentails.");
            message.put("Status", "Error");
            response = new ResponseEntity<String>(new Gson().toJson(message), HttpStatus.FORBIDDEN);
        }

        return response;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<User> updateUser(@RequestBody User user, @PathVariable Integer id) {
        // Modified code to support update User and role for the selected user.
		// 6Dec2017
		System.out.println("Inside update");
        userService.updateUser(user, id);
        return new ResponseEntity<User>( HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable Integer id) {
         userService.deleteUser(id);        
        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }
    @RequestMapping(value = "/loggedUsers", method = RequestMethod.GET)
    public ResponseEntity<List<String>> getLoggedUsersFromSessionRegistry() {
    	List<String> response= userService.getUsersFromSessionRegistry();
    	return new ResponseEntity<List<String>>( response,HttpStatus.OK);
    }
}
