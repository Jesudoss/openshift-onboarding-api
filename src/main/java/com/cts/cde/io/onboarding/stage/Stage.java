package com.cts.cde.io.onboarding.stage;

import com.cts.cde.io.onboarding.sub_stage.SubStage;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "stage")
public class Stage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @OneToMany(
            mappedBy = "stage",
            cascade = CascadeType.PERSIST,
            fetch = FetchType.LAZY,
            orphanRemoval = true)
    @JsonManagedReference
    @JsonIgnore
    @JsonProperty(value="subStages")
    private Set<SubStage> subStages;

    public Stage(int id, String name, Set<SubStage> subStages) {
        this.id = id;
        this.name = name;
        this.subStages = subStages;
    }

    public Stage() {
        super();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @JsonIgnore
    @JsonProperty(value="subStages")
    public Set<SubStage> getSubStages() {
        return subStages;
    }

    public void setSubStages(Set<SubStage> subStages) {
        this.subStages = subStages;
    }
}
