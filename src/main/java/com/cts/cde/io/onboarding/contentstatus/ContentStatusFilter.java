package com.cts.cde.io.onboarding.contentstatus;


public class ContentStatusFilter {

    private int id;
    private int user_id;
    private int content_id;

    private ContentStatus.Status status;

    public ContentStatusFilter(int id, int user_id, int content_id, ContentStatus.Status status) {
        this.id = id;
        this.user_id = user_id;
        this.content_id = content_id;
        this.status = status;
    }

    public ContentStatusFilter() {
        this.id = id;
        this.content_id = content_id;
        this.status = status;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getContent_id() {
        return content_id;
    }

    public void setContent_id(int content_id) {
        this.content_id = content_id;
    }

    public ContentStatus.Status getStatus() {
        return status;
    }

    public void setStatus(ContentStatus.Status status) {
        this.status = status;
    }
}
