package com.cts.cde.io.onboarding.usermanagement;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.cts.cde.io.onboarding.contentstatus.ContentStatus;
import com.cts.cde.io.onboarding.feedback.FeedBack;
import com.cts.cde.io.onboarding.role.Role;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "User")
public class User {

	private interface Table {
		String ID = "ID";
		String PASSWORD = "PASSWORD";
		String EMAIL_ID = "EMAIL_ID";
		String USER_NAME = "USER_NAME";
		String FIRST_NAME = "FIRST_NAME";
		String LAST_NAME = "LAST_NAME";
		String LANGUAGE = "LANGUAGE";
		String IS_ADMIN = "IS_ADMIN";
		
		
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = Table.ID)
	private Integer id;

	@Column(name = Table.PASSWORD)
	private String password;

	@Column(name = Table.EMAIL_ID, unique = true)
	private String emailId;

	@Column(name = Table.USER_NAME, unique = true)
	private String userName;

	@Column(name = Table.FIRST_NAME)
	private String firstName;

	@Column(name = Table.LAST_NAME)
	private String lastName;

	@Column(name = Table.LANGUAGE)
	private String language;

	@Column(name = Table.IS_ADMIN)
	private Boolean isAdmin;
	
	

	// 12/14/2017 -- Added for deleting the reference user contents

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonBackReference(value = "user_content_status")
	private Set<ContentStatus> userContentStatus;

	public User() {
	}

	public User(String userID, String userEmail, String password, String firstName, String lastName, String language,
			Boolean isAdmin) {
		this.emailId = userEmail;
		this.password = password;
		this.userName = userID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.language = language;
		this.isAdmin = isAdmin;
	}

	public User(int userID, String userEmail, String password, String firstName, String lastName, String language,
			Set<ContentStatus> userContentStatus, Boolean isAdmin) {
		this.emailId = userEmail;
		this.password = password;
		this.id = userID;
		this.userName = userEmail;
		this.firstName = firstName;
		this.lastName = lastName;
		this.language = language;
		this.isAdmin = isAdmin;
		this.userContentStatus = userContentStatus;
	}
	public User(int userID, String userEmail, String password, String firstName, String lastName, String language,
			 Boolean isAdmin) {
		this.emailId = userEmail;
		this.password = password;
		this.id = userID;
		this.userName = userEmail;
		this.firstName = firstName;
		this.lastName = lastName;
		this.language = language;
		this.isAdmin = isAdmin;
	}

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinColumn(name = "role_id")
	@JsonBackReference
	private Role role;
	
	@OneToMany(
            mappedBy = "user",
            cascade = CascadeType.PERSIST,
            fetch = FetchType.LAZY,
            orphanRemoval = true)
    @JsonManagedReference    
    @JsonIgnore
    private List<FeedBack> feedback = new ArrayList<>();

	public List<FeedBack> getFeedback() {
		return feedback;
	}

	public void setFeedback(List<FeedBack> feedback) {
		this.feedback = feedback;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String email_id) {
		this.emailId = email_id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String user_name) {
		this.userName = user_name;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Boolean getAdmin() {
		return isAdmin;
	}

	public void setAdmin(Boolean admin) {
		isAdmin = admin;
	}

	public Set<ContentStatus> getUserContentStatus() {
		return userContentStatus;
	}

	public void setUserContentStatus(Set<ContentStatus> userContentStatus) {
		this.userContentStatus = userContentStatus;
	}
	
}
