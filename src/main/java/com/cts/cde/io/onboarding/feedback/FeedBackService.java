package com.cts.cde.io.onboarding.feedback;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

@Service
public class FeedBackService {
	@Autowired
	public FeedBackRepository feedbackRepository;

	public List<FeedBack> getAllistFeedbacks() {
		return feedbackRepository.getAllistfeedbacks();
	}

	public List<FeedBack> getAllFeedbackList() {
		return feedbackRepository.getAllfeedbacks();
	}

	public List<FeedBack> getFeedbackAllById(Integer user_id) {
		return feedbackRepository.getAllfeedbacksById(user_id);
	}

	public Object getFeedbackById(Integer user_id) throws JSONException {
		int count = feedbackRepository.getfeedbackCount(user_id);
		JSONObject respose = new JSONObject();
		respose.put("count", count);
		Date maxDate=feedbackRepository.getmaxDate(user_id);
		List<FeedBack> feedbackString = feedbackRepository.getfeedback(user_id,maxDate);
		Iterator itr = feedbackString.iterator();
		while (itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			respose.put("feedback", String.valueOf(obj[0]));
			respose.put("feedback_head", String.valueOf(obj[1]));
			respose.put("emailId", String.valueOf(obj[2]));
			respose.put("created_date", String.valueOf(obj[3]));
			respose.put("isApproved", String.valueOf(obj[4]));
		}
		Gson gson = new Gson();
		Object obj = gson.fromJson(respose.toString(), Object.class);
		return obj;
	}

	@Transactional
	public void createUserFeedback(FeedBack feedback) {
		feedbackRepository.save(feedback);
	}

	@Transactional
	public void updateFeedback(FeedBack feedBack, Integer id) {
		feedbackRepository.upadteIsLatest(feedBack.getUser().getId());
		feedbackRepository.updateFeedback(feedBack.getIsFeedback(), id);
	}

	public void deleteFeedback(Integer id) {
		feedbackRepository.delete(id);
	}
}
