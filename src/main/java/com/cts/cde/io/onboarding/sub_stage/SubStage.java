package com.cts.cde.io.onboarding.sub_stage;

import com.cts.cde.io.onboarding.content.Content;
import com.cts.cde.io.onboarding.stage.Stage;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "sub_stage")
public class SubStage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "stage_id", nullable = false)
    @JsonBackReference
    private Stage stage;

    @OneToMany(
            mappedBy = "subStage",
            cascade = CascadeType.PERSIST,
            fetch = FetchType.LAZY,
            orphanRemoval = true)
    @JsonManagedReference
    @JsonIgnore
    @JsonProperty(value="contents")
    private Set<Content> contents;


    public SubStage() {
        super();
    }

    public SubStage(int id, String name, Stage stage, Set<Content> contents) {
        this.id = id;
        this.name = name;
        this.stage = stage;
        this.contents = contents;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @JsonIgnore
    @JsonProperty(value="contents")
    public Set<Content> getContents() {
        return contents;
    }

    public void setContents(Set<Content> contents) {
        this.contents = contents;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
