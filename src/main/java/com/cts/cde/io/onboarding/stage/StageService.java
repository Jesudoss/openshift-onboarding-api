package com.cts.cde.io.onboarding.stage;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StageService {


    @Autowired
    private StageRepository stageRepository;

    public List<Stage> getStageList() {

        List<Stage> stages = new ArrayList<>();
        stageRepository.findAll().forEach(stages::add);
        return stages;
    }

    public List<Stage> getStageListByNameAndRole(String name, Integer id) {
        List<Stage> stages = new ArrayList<>();
        stageRepository.findByStageAndRole(name, id).forEach(stages::add);
        return stages;
    }

    public Stage getStage(Integer id) {
        return stageRepository.findOne(id);
    }

    public Stage createStage(Stage stage) {
        return stageRepository.save(stage);
    }

    public Stage updateStage(Stage stage, Integer id) {
        return stageRepository.save(stage);
    }


    public void deleteStage(Integer id) {
        stageRepository.delete(id);

    }
}
