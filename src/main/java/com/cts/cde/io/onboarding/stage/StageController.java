package com.cts.cde.io.onboarding.stage;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/stages")
@CrossOrigin
public class StageController {

    @Autowired
    private StageService stageService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Stage>> getAllStages(@RequestParam(value = "name") Optional<String> name,
                                                    @RequestParam(value = "id") Optional<String> id) {
        List<Stage> stageData;
        if (id.isPresent() && id.isPresent()) {
            stageData = stageService.getStageListByNameAndRole(name.get(), Integer.parseInt(id.get()));
        } else {
            stageData = stageService.getStageList();
        }
        return new ResponseEntity<List<Stage>>(stageData, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Stage> getStage(@PathVariable Integer id) {

        Stage stageData = stageService.getStage(id);
        return new ResponseEntity<Stage>(stageData, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Stage> createStage(@RequestBody Stage stage) {

        Stage stageData = stageService.createStage(stage);
        return new ResponseEntity<Stage>(stageData, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Stage> updateStage(@RequestBody Stage stage,
                                             @PathVariable Integer id) {

        Stage stageData = stageService.updateStage(stage, id);
        return new ResponseEntity<Stage>(stageData, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<String> deleteStage(@PathVariable Integer id) {

        stageService.deleteStage(id);
        return new ResponseEntity<String>(HttpStatus.OK);
    }


}
