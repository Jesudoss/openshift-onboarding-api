package com.cts.cde.io.onboarding.sub_stage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sub-stages")
@CrossOrigin
public class SubStageController {
    @Autowired
    private SubStageService subStageService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<SubStageFilter>> getAllsubStages() {
        List<SubStageFilter> subStages = subStageService.getSubStageList();
        return new ResponseEntity<List<SubStageFilter>>(subStages, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<SubStage> getSubStage(@PathVariable Integer id) {
        SubStage subStage = subStageService.getSubStage(id);
        return new ResponseEntity<SubStage>(subStage, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<SubStage> createSubStage(@RequestBody SubStage subStage) {
        SubStage subStageData = subStageService.createSubStage(subStage);
        return new ResponseEntity<SubStage>(subStageData, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<SubStage> updateSubStage(@RequestBody SubStage subStage, @PathVariable Integer id) {
        System.out.println("Inside update");
        subStageService.updateSubStage(subStage, id);
        return new ResponseEntity<SubStage>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<String> deleteContent(@PathVariable Integer id) {
        subStageService.deleteSubStage(id);
        return new ResponseEntity<String>(HttpStatus.OK);
    }
}
