package com.cts.cde.io.onboarding.sub_stage;

import com.cts.cde.io.onboarding.stage.Stage;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SubStageRepository extends CrudRepository<SubStage,Integer>{
    @Query("SELECT NEW com.cts.cde.io.onboarding.sub_stage.SubStageFilter(SS.id, SS.name, SS.stage) FROM SubStage SS")
    List<SubStageFilter> findAllWithFilter();

    //Update stage id also in on 06/12/17
    @Modifying
    @Query("UPDATE SubStage SS SET SS.name = :subStageName, SS.stage = :stage WHERE SS.id= :subStageId")
    void updateSubStage(@Param("subStageName") String subStageName, @Param("subStageId") int subStageId,@Param("stage") Stage stage);
}
