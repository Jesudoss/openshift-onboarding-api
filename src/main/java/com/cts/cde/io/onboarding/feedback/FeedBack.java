package com.cts.cde.io.onboarding.feedback;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.cts.cde.io.onboarding.usermanagement.User;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "Feedback")
public class FeedBack {
	private interface Table {
		@SuppressWarnings("unused")
		String ID = "ID";
		String FEEDBACK_HEAD = "FEEDBACK_HEAD";
		String FEEDBACK = "FEEDBACK";
		String CREATED_DATE = "CREATED_DATE";
		String IS_FEEDBACK="IS_FEEDBACK";
		String IS_LATEST="IS_LATEST";
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	/*@Column(name = Table.USER_ID)
	private Integer user_id;*/
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinColumn(name = "user_id")
	@JsonBackReference
	private User user;

	@Column(name = Table.CREATED_DATE, nullable= false)
	private Date created_date;
	
	@Column(name = Table.FEEDBACK)
	private String feedback;
	
	@Column(name = Table.FEEDBACK_HEAD)
	private String feedback_head;


	@Column(name = Table.IS_FEEDBACK)
	private Boolean isFeedback;
	
	@Column(name = Table.IS_LATEST)
	private Boolean isLatest;

	public Boolean getIsLatest() {
		return isLatest;
	}

	public void setIsLatest(Boolean isLatest) {
		this.isLatest = isLatest;
	}

	public Boolean getIsFeedback() {
		return isFeedback;
	}

	public void setIsFeedback(Boolean isFeedback) {
		this.isFeedback = isFeedback;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public FeedBack(int id, User user_id, String feedback) {
		super();
		this.id = id;
		this.user = user_id;
		this.feedback = feedback;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_Date) {
		this.created_date = created_Date;
	}
	public String getFeedback_head() {
		return feedback_head;
	}

	public void setFeedback_head(String feedback_head) {
		this.feedback_head = feedback_head;
	}
	public FeedBack(User user_id, String feedback,Date created_Date) {
		super();
		this.user = user_id;
		this.feedback = feedback;
		this.created_date=created_Date;
	}
	public FeedBack(User user_id, String feedback) {
		super();
		this.user = user_id;
		this.feedback = feedback;
	}

	public FeedBack() {

	}

}
