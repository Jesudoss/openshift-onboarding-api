package com.cts.cde.io.onboarding.sub_stage;

import com.cts.cde.io.onboarding.stage.Stage;

public class SubStageFilter {
    public int id;
    public String name;
    public Stage stage;

    public SubStageFilter(int id, String name, Stage stage) {
        this.id = id;
        this.name = name;
        this.stage = stage;
    }
}
