package com.cts.cde.io.onboarding.usermanagement;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cts.cde.io.onboarding.role.ErrorMessage;

@Service
@Component
public class UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

//	@Value("${spring.mail.senderaddress}")
//	public String senderId;

//	public String subject = "Welcome to Bhodha Onboarding Application";
//	String welcomeUserName = "";

//	@Autowired
//	private JavaMailSender javaMailSender;

	@Autowired
	public UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
    private SessionRegistry sessionRegistry;

	public List<User> getUserList() {
		List<User> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);
		return users;

	}

	public User getUserByUserName(String userName) {
		return userRepository.findByUserName(userName);
	}

	public UserRole getUserByUserNamewithRole(String userName) {
		return userRepository.getUserDetailsWithRole(userName);
	}

	public ErrorMessage createUser(User user) {
		user.setPassword(getEncodedPassword(user.getPassword()));
		User createdUser;
		ErrorMessage errorMessage = new ErrorMessage();
		int count = userRepository.findByEmailId(user.getEmailId());
		if (count <= 0) {
			
			createdUser = userRepository.save(user);
			errorMessage.setStatus("Success");
		}else {
			errorMessage.setStatus("Failed");
			errorMessage.setDescription(user.getEmailId()+" Already Exist.");
			
		}

		/*
		 * if(createdUser != null) { new Thread(() -> { try {
		 * sendEmailNotification(createdUser); } catch (InterruptedException e) {
		 * logger.info(e.getMessage()); } }).start(); }
		 */

		return errorMessage;
	}

	@Transactional
	public void updateUser(User user, Integer user_id) {
		userRepository.updateUser(user.getRole().getId(), user_id, user.getFirstName(), user.getLastName(),
				user.getUserName(), user.getEmailId(), user.getLanguage(), user.getAdmin());
	}

	public User authenticateUser(String userName, String password) {
		User users = userRepository.findByUserName(userName);
		if (users != null && checkPassword(password, users))
			return users;
		return null;
	}

	private String getEncodedPassword(String password) {
		return bCryptPasswordEncoder.encode(password);
	}

	private boolean checkPassword(String password, User users) {
		return bCryptPasswordEncoder.matches(password, users.getPassword());
	}

	public void deleteUser(Integer id) {
		userRepository.delete(id);
	}

//	@Async
//	public void sendEmailNotification(User user) throws MailException, InterruptedException {
//
//		SimpleMailMessage mail = new SimpleMailMessage();
//		mail.setTo(user.getEmailId());
//		mail.setFrom(senderId);
//		mail.setSubject(subject);
//		welcomeUserName = "Hi " + user.getFirstName() + ",";
//		mail.setText(welcomeUserName + "\n" + getContent());
//		javaMailSender.send(mail);
//
//	}

	public String getContent() {
		String content = "";
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("EmailTemplate.txt").getFile());
		try {
			content = new String(Files.readAllBytes(file.toPath()));
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
		return content;
	}
	    public List<String> getUsersFromSessionRegistry() {
	        return sessionRegistry.getAllPrincipals().stream().filter((u) -> !sessionRegistry.getAllSessions(u, false).isEmpty()).map(Object::toString).collect(Collectors.toList());
	    }
}
