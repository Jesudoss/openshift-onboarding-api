package com.cts.cde.io.onboarding.role;

import com.cts.cde.io.onboarding.usermanagement.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "STATUS") //TODO Need to make it as ENUM
    private String status;

    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(
            mappedBy = "role",
            cascade = CascadeType.PERSIST,
            fetch = FetchType.LAZY,
            orphanRemoval = true)
    @JsonManagedReference    
    @JsonIgnore
    private List<User> users = new ArrayList<>();

    public Role() {
    }

    public Role(Integer id, String name, String status, String description, List<User> users) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.description = description;
        this.users = users;
    }

    public Role(String role_name, String status, String description) {
        this.name = role_name;
        this.status = status;
        this.description = description;
    }
    
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
