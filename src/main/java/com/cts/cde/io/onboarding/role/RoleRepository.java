package com.cts.cde.io.onboarding.role;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.cts.cde.io.onboarding.content.Content;


public interface  RoleRepository extends CrudRepository<Role,Integer>{
   
   //12/12/2017 - Role delete query to return COUNT of roles with content
   String CUSTOM_QUERY_DELETE_CONTENT_ROLE = "SELECT COUNT(CT) " +
           "FROM Content CT JOIN CT.roles R "+
           "WHERE R.id= :role_id" ;

	
    @Query(CUSTOM_QUERY_DELETE_CONTENT_ROLE)
    int deleteByRoleId(@Param("role_id") int role_id);
}