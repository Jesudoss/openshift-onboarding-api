package com.cts.cde.io.onboarding.content;

public class ContentFilter {

    private int id;
    private String name;
    private String subStageName;
    private String stageName;
    private int displayOrder;

    public ContentFilter() {
    }

    public ContentFilter(int id, String name, String subStageName, String stageName,int displayOrder) {
        this.id = id;
        this.name = name;
        this.subStageName = subStageName;
        this.stageName = stageName;
        this.displayOrder=displayOrder;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public String getSubStageName() {
        return subStageName;
    }

    public void setSubStageName(String subStageName) {
        this.subStageName = subStageName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



}
