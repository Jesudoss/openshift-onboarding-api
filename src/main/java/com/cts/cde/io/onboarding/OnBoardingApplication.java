package com.cts.cde.io.onboarding;

//import org.flywaydb.core.Flyway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@SpringBootApplication
@EnableAsync
public class OnBoardingApplication {

    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }
//    @Bean
//    public FlywayMigrationInitializer flywayInitializer(Flyway flyway) {
//    	//flyway.baseline();
//   	    flyway.migrate();
//        return new FlywayMigrationInitializer(flyway);
//    }

	public static void main(String[] args) {
        SpringApplication.run(OnBoardingApplication.class, args);
    }
}