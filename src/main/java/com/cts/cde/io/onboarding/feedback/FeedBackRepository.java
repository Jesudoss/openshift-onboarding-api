package com.cts.cde.io.onboarding.feedback;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface FeedBackRepository extends CrudRepository<FeedBack, Integer> {
	   
   @Query("SELECT FD.feedback FROM FeedBack FD WHERE FD.user.id=:userId") 
    List<String> getfeedbackById(@Param("userId") int userId);
   
   @Query("SELECT FD.id,FD.feedback,FD.feedback_head,FD.user.id,FD.user.userName,FD.user.firstName,FD.user.lastName,FD.created_date,FD.isFeedback FROM FeedBack FD ORDER BY FD.created_date DESC") 
   List<FeedBack> getAllfeedbacks(); 
   
   @Query("SELECT FD.id,FD.feedback,FD.feedback_head,FD.user.id,FD.user.userName,FD.user.firstName,FD.user.lastName,FD.created_date,FD.isFeedback FROM FeedBack FD WHERE FD.user.id=:userId and FD.isFeedback=true ORDER BY FD.created_date DESC") 
   List<FeedBack> getAllfeedbacksById(@Param("userId") int userId); 
   
  @Query("SELECT COUNT(FD.feedback) FROM FeedBack FD WHERE FD.user.id=:userId and FD.isFeedback=true") 
   int getfeedbackCount(@Param("userId") int userId); 
   
   @Query("SELECT FD.feedback,FD.feedback_head,FD.user.userName,FD.created_date,FD.isFeedback FROM FeedBack FD WHERE FD.user.id=:userId and FD.created_date=:created_Date")
   List<FeedBack> getfeedback(@Param("userId") int userId,@Param("created_Date") Date created_Date); 
   
   @Query("SELECT MAX(FD.created_date) FROM FeedBack FD WHERE FD.user.id=:userId") 
   Date getmaxDate(@Param("userId") int userId); 
   
  @Query("SELECT FD.feedback,FD.feedback_head,FD.user.id,FD.user.userName,FD.user.firstName,FD.user.lastName,FD.created_date,FD.isFeedback FROM FeedBack FD WHERE FD.isFeedback=true and FD.isLatest=true GROUP BY FD.user.id ORDER BY FD.created_date DESC") 
   List<FeedBack> getAllistfeedbacks();
   
   
   @Modifying
   @Query("UPDATE FeedBack as FD SET FD.isFeedback=:isFeedback,FD.isLatest=true WHERE FD.id= :id")
   void updateFeedback(@Param("isFeedback") Boolean isFeedback,@Param("id") int id);
   
   @Modifying
   @Query("UPDATE FeedBack as FD SET FD.isLatest=false WHERE FD.user.id= :userId")
   void upadteIsLatest(@Param("userId") int userId);
   
   
   

}
