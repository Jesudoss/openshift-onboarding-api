package com.cts.cde.io.onboarding.role;


//12/12/2017 - ErrorMessage class to handle return for ROLE delete
public class ErrorMessage {

    private String status;
    private String description;


    public ErrorMessage(String status, String description) {
        this.status = status;
        this.description = description;
    }

    public ErrorMessage() {


    }

    public String getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
