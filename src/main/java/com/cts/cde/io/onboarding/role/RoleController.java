package com.cts.cde.io.onboarding.role;

import java.util.List;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/role")
@CrossOrigin
public class RoleController {

    @Autowired
    private RoleService roleService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Role>> getAllRole() {
        List<Role> roleData = roleService.getRoleList();
        return new ResponseEntity<List<Role>>(roleData, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Role> getRole(@PathVariable Integer id) {
        Role roleData = roleService.getRole(id);
        return new ResponseEntity<Role>(roleData, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Role> createRole(@RequestBody Role role) {
        Role roleData = roleService.createRole(role);
        return new ResponseEntity<Role>(roleData, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Role> updateRole(@RequestBody Role role, @PathVariable Integer id) {
        Role roleData = roleService.updateRole(role, id);
        return new ResponseEntity<Role>(roleData, HttpStatus.OK);
    }

	//12/12/2017 - Role delete method modified to display message
	// If Content is present for that ROLE , then that particular ROLE cannot be deleted.
	// Admin user has to delete / remove content for that role.
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<String> deleteRole(@PathVariable Integer id) {
        ErrorMessage isDeleted = roleService.deleteRole(id);
        String errorMessage = new Gson().toJson(isDeleted);
        return new ResponseEntity<String>(errorMessage, HttpStatus.OK);
    }


}
