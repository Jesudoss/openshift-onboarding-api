package com.cts.cde.io.onboarding.contentstatus;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ContentStatusRepository extends CrudRepository<ContentStatus, Integer> {
     String CUSTOM_QUERY = "SELECT NEW com.cts.cde.io.onboarding.contentstatus.ContentStatusFilter(" +
             "CS.id,CS.user.id,CS.content.id, CS.status)" +
             " FROM ContentStatus CS where CS.user.id = :id";
     @Query(CUSTOM_QUERY)
     List<ContentStatusFilter> findByUser(@Param("id") int id);
}
