package com.cts.cde.io.onboarding.contentstatus;


import com.cts.cde.io.onboarding.content.Content;
import com.cts.cde.io.onboarding.usermanagement.User;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "user_content_status")
public class ContentStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "content_id", nullable = false)
    @JsonBackReference
    private Content content;


    public ContentStatus(){}

    public ContentStatus(int id, User user, Content content, Status status) {
        this.id = id;
        this.user = user;
        this.content = content;
        this.status = status;
    }

    public enum Status {
        INPROGRESS,
        COMPLETED
    }

    private Status status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
