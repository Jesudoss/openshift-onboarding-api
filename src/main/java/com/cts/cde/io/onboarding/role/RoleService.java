package com.cts.cde.io.onboarding.role;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public List<Role> getRoleList() {
        List<Role> roles = new ArrayList<>();
        roleRepository.findAll().forEach(roles::add);
        return roles;
    }

    public Role getRole(Integer id) {
        return roleRepository.findOne(id);
    }

    public Role createRole(Role role) {
        return roleRepository.save(role);
    }

    public Role updateRole(Role role, Integer id) {
        // Update ROLE with all the information.
        //6Dec2017
        Role toBeUpdRole = roleRepository.findOne(id);
        if (toBeUpdRole != null) {
            toBeUpdRole.setDescription(role.getDescription());
            toBeUpdRole.setId(id);
            toBeUpdRole.setStatus(role.getStatus());
            toBeUpdRole.setName(role.getName());

        }

        return roleRepository.save(toBeUpdRole);
        // Update ROLE with all the information.
        //6Dec2017

    }

	//12/12/2017 - Role delete based on content
    public ErrorMessage deleteRole(Integer id) {

        ErrorMessage errorMessage = new ErrorMessage();
        int value=roleRepository.deleteByRoleId(id);
        if (roleRepository.deleteByRoleId(id) > 0) {
            errorMessage.setStatus("Failed");
            errorMessage.setDescription("Content is associated with role");
        } else {
            errorMessage.setStatus("Success");
            errorMessage.setDescription("Successfully Deleted");
            roleRepository.delete(id);
        }
        return errorMessage;
    }

}
