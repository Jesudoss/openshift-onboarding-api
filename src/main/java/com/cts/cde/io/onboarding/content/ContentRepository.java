package com.cts.cde.io.onboarding.content;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import com.cts.cde.io.onboarding.contentstatus.ContentStatus;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

public interface ContentRepository extends CrudRepository<Content, Integer> {


    String CUSTOM_QUERY_BY_ROLE = "SELECT NEW com.cts.cde.io.onboarding.content.ContentFilter(" +
            "SC.id,SC.name,SC.subStage.name,SC.subStage.stage.name,SC.displayOrder)" +
            " FROM Content SC INNER JOIN SC.roles RS " +
            "INNER JOIN RS.users US WHERE US.userName=:name";

    String CUSTOM_QUERY_FOR_ADMIN = "SELECT NEW com.cts.cde.io.onboarding.content.ContentFilter(" +
            "SC.id,SC.name,SC.subStage.name,SC.subStage.stage.name,SC.displayOrder)" +
            " FROM Content SC";

    String CUSTOM_QUERY_FOR_DELETE = "DELETE FROM ContentStatus CS " +
            "WHERE CS.content.id= :contentId";

    String CUSTOM_CONTENT_COUNT="SELECT COUNT(*) FROM Content CT JOIN CT.subStage SS where SS.id=:subStageId";

    String CUSTOM_QUERY_FOR_DISPLAYORDER_UPDATE="UPDATE Content as CT SET CT.displayOrder=:displayOrder where CT.id=:contentId";

    @Query(CUSTOM_QUERY_BY_ROLE)
    List<ContentFilter> findByRoleId(@Param("name") String name);

    @Query(CUSTOM_QUERY_FOR_ADMIN)
    List<ContentFilter> findAllForAdmin();

    @Transactional
    @Modifying
    @Query(CUSTOM_QUERY_FOR_DELETE)
    void deleteAllUserContentStatus(@Param("contentId") int contentId);

     @Query(CUSTOM_CONTENT_COUNT)
    int countContent(@Param("subStageId") int substageID);

     @Transactional
     @Modifying
     @Query(CUSTOM_QUERY_FOR_DISPLAYORDER_UPDATE)
     void  updateContentOrder(@Param("contentId")int contentId,@Param("displayOrder")int displayOrder);
}
