package com.cts.cde.io.onboarding.usermanagement;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.cts.cde.io.onboarding.role.ErrorMessage;
import com.cts.cde.io.onboarding.role.Role;

public class UserServiceTest {

    @InjectMocks
    private UserService service;

    @Mock
    private UserRepository repository;
    
    @Mock
    private JavaMailSender javaMailSender;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    String expectedUserName = "someuser";
    private static final String SOMEPASSWORD = "somepassword";
    User sampleUser = new User(expectedUserName,
            "someUserEmail" + System.currentTimeMillis(),
            SOMEPASSWORD, "firstName",
            "lastName", "english",false);


    @Test
    public void thatGetListOfUsersWhenRequestAllUsers() {
        //Given
        List<User> mockData = new ArrayList<>();
        mockData.add(sampleUser);
        when(repository.findAll()).thenReturn(mockData);
        List<User> client = service.getUserList();
        assertEquals(1, client.size());
    }

    @Test
    public void thatGetUserWhenRequestUserByUserName() {
        sampleUser.setRole(new Role());
        when(repository.findByUserName(expectedUserName)).thenReturn(sampleUser);
        User response = service.getUserByUserName(expectedUserName);
        assertThat(response.getUserName(), is(expectedUserName));
        assertThat(response.getRole(), is(notNullValue()));
    }

    @Test
    public void thatEmptyUseWhenRequestInvalidUser() {
        when(repository.findByUserName(expectedUserName)).thenReturn(sampleUser);
        User client = service.getUserByUserName("dummy");
        assertNull(client);
    }


    @Test
    public void thatCreateNewUserWhenPassingNewUserInfo() {
        when(repository.save(sampleUser)).thenReturn(sampleUser);
        String encodedpassword = "encodedpassword";
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setStatus("Success");
        when(bCryptPasswordEncoder.encode(sampleUser.getPassword()))
                .thenReturn(encodedpassword);

        
        ErrorMessage status = service.createUser(sampleUser);
        assertEquals(status.getStatus(),"Success");
    }

    @Test
    public void thatUpdateUserWhenPassingUpdateValueUserInfo() {
        when(repository.save(sampleUser)).thenReturn(sampleUser);
       // User client = service.updateUser(sampleUser, 1);
      //  assertEquals(client.getUserName(), expectedUserName);
    }


    @Test
    public void thatAuthendicateUserWhenPassingUserNameAndPlainPassword() {

        String encodedpassword = "encodedpassword";
        when(bCryptPasswordEncoder.encode(sampleUser.getPassword()))
                .thenReturn(encodedpassword);
        sampleUser.setPassword(bCryptPasswordEncoder.encode(SOMEPASSWORD));

        when(repository.findByUserName(expectedUserName)).thenReturn(sampleUser);
        when(bCryptPasswordEncoder.matches(SOMEPASSWORD, encodedpassword)).thenReturn(true);

        User client = service.authenticateUser(expectedUserName, SOMEPASSWORD);


        assertEquals(client.getUserName(), expectedUserName);
    }

    @Test
    public void thatAuthendicateUserWhenPassingValidUserNameAndInvalidPlainPassword() {

        String encodedpassword = "encodedpassword";
        when(bCryptPasswordEncoder.encode(sampleUser.getPassword()))
                .thenReturn(encodedpassword);
        sampleUser.setPassword(bCryptPasswordEncoder.encode(SOMEPASSWORD));

        when(repository.findByUserName(expectedUserName)).thenReturn(sampleUser);
        when(bCryptPasswordEncoder.matches(SOMEPASSWORD, encodedpassword)).thenReturn(false);

        User client = service.authenticateUser(expectedUserName, SOMEPASSWORD);
        assertNull(client);
    }

    @Test
    public void thatAuthendicateUserWhenPassingInvalidUserNameAndPlainPassword() {

        String encodedpassword = "encodedpassword";
        when(bCryptPasswordEncoder.encode(sampleUser.getPassword()))
                .thenReturn(encodedpassword);
        sampleUser.setPassword(bCryptPasswordEncoder.encode(SOMEPASSWORD));

        when(repository.findByUserName(expectedUserName)).thenReturn(null);
        when(bCryptPasswordEncoder.matches(SOMEPASSWORD, encodedpassword)).thenReturn(false);

        User client = service.authenticateUser(expectedUserName, SOMEPASSWORD);
        assertNull(client);
    }

	
	// TDD for Valid Email Address
  /*  @Test
    public void thatUserEmailAddressIsNotNull(){
        String actualEmailAddress = "someUserEmail";
        String expectedEmailAddress = "";
        when(repository.findByUserName(expectedUserName)).thenReturn(sampleUser);
        sampleUser.setEmailId("someUserEmail");
        if(sampleUser.getEmailId() == "someUserEmail")
            expectedEmailAddress = "someUserEmail";
        assertEquals(expectedEmailAddress, actualEmailAddress);
    }
    @Test
    public void thatUserEmailAddressIsNull(){
        String actualEmailAddress = "";
        String expectedEmailAddress = "";
        when(repository.findByUserName(expectedUserName)).thenReturn(sampleUser);
        sampleUser.setEmailId("");
        if(sampleUser.getEmailId() == "")
            expectedEmailAddress = "";
        assertEquals(expectedEmailAddress, actualEmailAddress);
    }*/

}