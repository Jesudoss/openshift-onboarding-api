package com.cts.cde.io.onboarding.feedback;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import com.cts.cde.io.onboarding.OnBoardingApplication;
import com.cts.cde.io.onboarding.usermanagement.User;
import com.google.gson.Gson;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;



@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OnBoardingApplication.class)
@WebMvcTest(value = FeedBackController.class, secure = false)
public class FeedBackControllerTest {

	public static final String CONTENT_TYPE = "Content-type";
    public static final String FEEDBACK="/feedback";
    public static final ResultMatcher OK = status().isOk();
    public static final ResultMatcher RESPONSE_FORMAT = content().contentType(MediaType.APPLICATION_JSON_UTF8);

//	@MockBean
//	private FlywayMigrationInitializer FlywayMigrationInitializer;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	FeedBackController feedbackController;

	@MockBean
	FeedBackService feedbackService;
	
	 User sampleUser = new User("username",
	            "someUserEmail" + System.currentTimeMillis(),
	            "password",  "firstName",
	            "lastName", "english",false);

	FeedBack sampleFeedback= new FeedBack(sampleUser,"samplefeedback");
	
	
	@Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.feedbackController).build();
        MockitoAnnotations.initMocks(this);
    }
	
	
	@Test
	public void testGetAllFeedbacks() throws Exception {
		List<FeedBack> feedbackList = new ArrayList<FeedBack>();
		feedbackList.add(sampleFeedback);
        given(feedbackService.getAllistFeedbacks()).willReturn(feedbackList);

        ResultMatcher hasRecord = jsonPath("$").isArray();
        MvcResult result = this.mockMvc.perform(get(FEEDBACK)
                .accept(MediaType.APPLICATION_JSON).header(CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(OK).andExpect(RESPONSE_FORMAT)
                .andExpect(hasRecord).andReturn();

        String jsonString = result.getResponse().getContentAsString();
        DocumentContext context = JsonPath.parse(jsonString);
        int length = context.read("$.length()");
        assertThat(1, is(length));
	}

	@Test
	public void testGetAllFeedbackById() throws Exception {
		List<FeedBack> feedbackList = new ArrayList<FeedBack>();
		feedbackList.add(sampleFeedback);
        given(feedbackService.getAllFeedbackList()).willReturn(feedbackList);

        ResultMatcher hasRecord = jsonPath("$").isArray();
        MvcResult result = this.mockMvc.perform(get(FEEDBACK+"/all")
                .accept(MediaType.APPLICATION_JSON).header(CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(OK).andExpect(RESPONSE_FORMAT)
                .andExpect(hasRecord).andReturn();

        String jsonString = result.getResponse().getContentAsString();
        DocumentContext context = JsonPath.parse(jsonString);
        int length = context.read("$.length()");
        assertThat(1, is(length));
	}

	@Test
	public void testCreateUserFeedback() throws Exception {
        String content = new Gson().toJson(sampleFeedback);
        this.mockMvc.perform(post(FEEDBACK)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(OK)
                .andExpect(OK)
                .andReturn();
	}

}
