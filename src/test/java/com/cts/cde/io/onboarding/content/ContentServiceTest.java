package com.cts.cde.io.onboarding.content;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.cts.cde.io.onboarding.role.Role;
import com.cts.cde.io.onboarding.sub_stage.SubStage;
import com.cts.cde.io.onboarding.usermanagement.User;
import com.cts.cde.io.onboarding.usermanagement.UserRepository;

public class ContentServiceTest {


    @InjectMocks
    ContentService service;

    @Mock
    ContentRepository repository;
    
//    @MockBean
//    private FlywayMigrationInitializer flywayMigrationInitializer;
    @Mock
    UserRepository userRepository;

    private String SOME_NAME = "some_name";

    User user = new User("sree",
            "someUserEmail" + System.currentTimeMillis(),
            "somePassword", "firstName",
            "lastName", "english",false);
    private Set<Role> roles = new HashSet() {
        {
            add(new Role());
        }
    };
    private List<Document> documents = new ArrayList() {{
        add(new Document(1, Document.Type.PDF, "", null));
    }};



    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void thatGetAllContentWhenCallGetAllContent() {

        List<Content> contents = new ArrayList<>();

        contents.add(new Content(1, SOME_NAME, new SubStage()
                , roles, "TITLE", "HEADER",
                "DESC", "FOOTER",
                new Date(System.currentTimeMillis()), documents,1));

        when(repository.findAll()).thenReturn(contents);
        List<Content> client = service.getContentList();
        assertEquals(1, client.size());


    }

    @Test
    public void thatGetContentWhenCallGetContentById() {

        Content content = new Content(1, SOME_NAME, new SubStage()
                , roles, "TITLE", "HEADER",
                "DESC", "FOOTER",
                new Date(System.currentTimeMillis()), documents,1);

        when(repository.findOne(1)).thenReturn(content);
        Content client = service.getContent(1);
        assertThat(client.getName(), is(SOME_NAME));


    }

    @Test
    public void thatGetContentWhenCallGetContentByRoleId() {
        List<ContentFilter> contentFilter = new ArrayList();
        contentFilter.add(
                new ContentFilter(1, SOME_NAME, "SUB_STAGE_NAME", "STAGE_NAME",1));
        when(repository.findByRoleId(SOME_NAME)).thenReturn(contentFilter);
        when(userRepository.findByUserName(SOME_NAME)).thenReturn(user);
        List<ContentFilter> response = service.getContentByRoleId(SOME_NAME);
        assertThat(response.size(), is(1));
    }

    @Test
    public void thatCreateNewContentWhenPassingContentInfo(){

        Content content = new Content(1, SOME_NAME, new SubStage()
                , roles, "TITLE", "HEADER",
                "DESC", "FOOTER",
                new Date(System.currentTimeMillis()), documents,1);
        content.getSubStage().setId(1);
        when(repository.save(content)).thenReturn(content);
        Content response = service.createContent(content);
        assertThat(response.getName(), is(SOME_NAME));

    }

    @Test
    public void thatUpdateContentWhenPassingContentInfo(){

        Content content = new Content(1, SOME_NAME, new SubStage()
                , roles, "TITLE", "HEADER",
                "DESC", "FOOTER",
                new Date(System.currentTimeMillis()), documents,1);
         content.getSubStage().setId(1);
        when(repository.save(content)).thenReturn(content);
        Content response = service.updateContent(content);
        assertThat(response.getName(), is(SOME_NAME));

    }

    @Test
    public void thatDeleteContentWhenPassingContentId() {
        Content content = new Content(1, SOME_NAME, new SubStage()
                , roles, "TITLE", "HEADER",
                "DESC", "FOOTER",
                new Date(System.currentTimeMillis()), documents,1);
        when(repository.findOne(1)).thenReturn(content);
        service.deleteContent(1);
        assertTrue(true);
    }
}