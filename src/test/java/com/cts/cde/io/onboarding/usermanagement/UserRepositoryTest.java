package com.cts.cde.io.onboarding.usermanagement;

import com.cts.cde.io.onboarding.role.Role;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository repository;
    
//    @MockBean
//    private FlywayMigrationInitializer flywayMigrationInitializer;

    String expectedUserName = "someuser";
    User user = new User(expectedUserName,
            "someUserEmail" + System.currentTimeMillis(),
            "somePassword", "firstName",
            "lastName", "english",true);
    Role sampleRole = new Role("role_name", "Active", "Description sample");

    @Before
    public void setup() {
        user.setRole(sampleRole);
    }

    @Test
    public void thatGetListOfUsersWhenRequestAllUsers() {
        entityManager.merge(user);
        List<User> client = (List<User>) repository.findAll();
        assertEquals(1, client.size());
    }


    @Test
    public void thatGetUserWhenPassingUserName() {
        entityManager.merge(user);
        User client = (User) repository.findByUserName(expectedUserName);
        String actual = client.getUserName();
        System.out.println("thatGetUserWhenPassingUserName::actual::" + actual +
                "::expectedUserName::" + expectedUserName);
        assertEquals(actual, expectedUserName);
    }
}