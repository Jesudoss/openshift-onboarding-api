package com.cts.cde.io.onboarding.RoleManagement;


import com.cts.cde.io.onboarding.role.Role;
import com.cts.cde.io.onboarding.role.RoleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RoleRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RoleRepository repository;
    
//    @MockBean
//    private FlywayMigrationInitializer flywayMigrationInitializer;

    @Test
    public void thatGetListOfRolesWhenRequestAllRoles() {
        entityManager.persist(new Role());
        List<Role> client = (List<Role>) repository.findAll();
        assertEquals(1, client.size());
    }


    @Test
    public void thatDeleteRoleWhenRequestDeleteRoleById() {
        entityManager.persist(new Role());
        List<Role> savedData = (List<Role>) repository.findAll();
        repository.delete(savedData.get(0).getId());
        List<Role> client = (List<Role>) repository.findAll();
        assertEquals(0, client.size());
    }



}
