package com.cts.cde.io.onboarding.contentstatus;


import com.cts.cde.io.onboarding.OnBoardingApplication;
import com.cts.cde.io.onboarding.content.Content;
import com.cts.cde.io.onboarding.sub_stage.SubStage;
import com.cts.cde.io.onboarding.usermanagement.User;
import com.google.gson.Gson;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OnBoardingApplication.class)
@WebMvcTest(value = ContentStatusController.class, secure = false)
public class ContentStatusControllerTest {

    public static final String CONTENT_TYPE = "Content-type";
    public static final ResultMatcher OK = status().isOk();
    public static final ResultMatcher RESPONSE_FORMAT = content().contentType(MediaType.APPLICATION_JSON_UTF8);

    private MockMvc mockMvc;

    @MockBean
    ContentStatusService service;

    @Autowired
    private ContentStatusController contentStatusController;
    
//    @MockBean
//    private FlywayMigrationInitializer flywayMigrationInitializer;
    
    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.contentStatusController).build();
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void thatGetContentStatusWhenPassingContentIdAndUserId() throws Exception {
        ContentStatusFilter contentStatus = getContentStatusFilter();
        List<ContentStatusFilter> list = new ArrayList();
        list.add(contentStatus);
        given(service.getContentStatusByUserId(2)).willReturn(list);

        ResultMatcher hasRecord = jsonPath("$").isArray();
        MvcResult result = this.mockMvc.perform(get("/content-status/2")
                .accept(MediaType.APPLICATION_JSON).header(CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(OK).andExpect(RESPONSE_FORMAT)
                .andExpect(hasRecord)
                .andReturn();
        Mockito.verify(service,Mockito.atLeastOnce()).getContentStatusByUserId(eq(2));

        String jsonString = result.getResponse().getContentAsString();
        DocumentContext context = JsonPath.parse(jsonString);
        int length = context.read("$.length()");
        assertThat(length, is(1));
        int userId = context.read("$[0].user_id");
        assertThat(userId, is(1));
        int contentId = context.read("$[0].content_id");
        assertThat(contentId, is(1));
        assertThat(context.read("$[0].id"), is(notNullValue()));
        assertThat(context.read("$[0].status"), is(notNullValue()));
    }

    private ContentStatusFilter getContentStatusFilter() {
        ContentStatusFilter contentStatus = new ContentStatusFilter();
        User user = new User("USERID", "USERID",
                "PWD", "FIRSTNAME",
                "LAST_NAME", "EN",true);
        user.setId(1);
        contentStatus.setUser_id(user.getId());
        contentStatus.setId(1);
        contentStatus.setStatus(ContentStatus.Status.COMPLETED);
        Content content = new Content(1, "ABC", new SubStage(),1);
        contentStatus.setContent_id(content.getId());
        return contentStatus;
    }


    private ContentStatus getContentStatus() {
        ContentStatus contentStatus = new ContentStatus();
        User user = new User("USERID", "USERID",
                "PWD", "FIRSTNAME",
                "LAST_NAME", "EN",true);
        user.setId(1);
        contentStatus.setUser(user);
        contentStatus.setId(1);
        contentStatus.setStatus(ContentStatus.Status.COMPLETED);
        Content content = new Content(1, "ABC", new SubStage(),1);
        contentStatus.setContent(content);
        return contentStatus;
    }

    @Test
    public void thatCreateContentStatusWhenByPassingContentStatus() throws Exception {

        given(service.createUserContentStatus(Mockito.any(ContentStatus.class))).willReturn(getContentStatus());

        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.post("/content-status")
                .content(new Gson().toJson(getContentStatus()))
                .accept(MediaType.APPLICATION_JSON).header(CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        Mockito.verify(service,Mockito.atLeastOnce()).createUserContentStatus(Mockito.any(ContentStatus.class));
    }


    @Test
    public void thatGettingBadRequestCreateContentStatusWhenByPassingInvalidContentStatus() throws Exception {

        given(service.createUserContentStatus(Mockito.any(ContentStatus.class))).willReturn(null);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/content-status")
                .accept(MediaType.APPLICATION_JSON).header(CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

}

