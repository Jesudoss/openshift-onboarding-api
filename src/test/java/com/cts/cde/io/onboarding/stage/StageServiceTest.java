package com.cts.cde.io.onboarding.stage;

import com.cts.cde.io.onboarding.sub_stage.SubStage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class StageServiceTest {


    @InjectMocks
    private StageService service;
    private String SOME_NAME = "somename";
    private static final int SOME_ID = 1;

    @Mock
    private StageRepository repository;

    private Set<SubStage> subStages = new HashSet() {
        {
            add(new SubStage());
        }
    };
    private Stage stage = new Stage(1, SOME_NAME, subStages);

    @Before
    public void setup() {
        stage.setName(SOME_NAME);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void thatGetListOfStagesWhenRequestAllStages() {
        //Given
        List<Stage> mockData = new ArrayList<>();
        mockData.add(stage);
        when(repository.findAll()).thenReturn(mockData);
        List<Stage> list = service.getStageList();
        assertEquals(1, list.size());
    }

    @Test
    public void thatGetListOfStagesWhenRequestAllStagesByRoleId() {
        //Given
        List<Stage> mockData = new ArrayList<>();
        mockData.add(stage);
        when(repository.findByStageAndRole(SOME_NAME,SOME_ID)).thenReturn(mockData);
        List<Stage> list = service.getStageListByNameAndRole(SOME_NAME,SOME_ID);
        assertEquals(1, list.size());
    }

    @Test
    public void thatGetParticularStageWhenRequestByStageId() {

        when(repository.findOne(1)).thenReturn(stage);
        Stage client = service.getStage(1);
        assertEquals(client.getName(), SOME_NAME);
    }


    @Test
    public void thatCreateStageWhenPassingNewStageInfo() {
        when(repository.save(stage)).thenReturn(stage);
        Stage subStage = service.createStage(stage);
        assertEquals(subStage.getName(), SOME_NAME);
    }

    @Test
    public void thatUpdateStageWhenPassingUpdatedStagesInfo() {
        when(repository.save(stage)).thenReturn(stage);
        Stage client = service.updateStage(stage, 1);
        assertEquals(client.getName(), SOME_NAME);
    }

    @Test
    public void thatDeleteRoleWhenPassingStageId() {
        service.deleteStage(1);
        assertTrue(true);
    }


}