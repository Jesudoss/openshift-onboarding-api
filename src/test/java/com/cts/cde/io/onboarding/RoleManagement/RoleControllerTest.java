package com.cts.cde.io.onboarding.RoleManagement;


import com.cts.cde.io.onboarding.OnBoardingApplication;
import com.cts.cde.io.onboarding.role.Role;
import com.cts.cde.io.onboarding.role.RoleController;
import com.cts.cde.io.onboarding.role.RoleService;
import com.google.gson.Gson;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OnBoardingApplication.class)
@WebMvcTest(value = RoleController.class, secure = false)
public class RoleControllerTest {


    public static final String CONTENT_TYPE = "Content-type";
    public static final ResultMatcher OK = status().isOk();
    public static final ResultMatcher RESPONSE_FORMAT = content().contentType(MediaType.APPLICATION_JSON_UTF8);

    public static final String PATH = "/role";
    public static final String SOME_ROLE_NAME = "some_role_name";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    RoleController controller;

    @MockBean
    RoleService service;
    
//    @MockBean
//    private FlywayMigrationInitializer flywayMigrationInitializer;

    private Role sampleRole = new Role(SOME_ROLE_NAME, "Active", "Description sample");

    @Before
    public void setUp() throws Exception {
        sampleRole.setId(1);
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.controller).build();
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void thatGetListOfRolesWhenFetchRoleList() throws Exception {

        List<Role> userList = new ArrayList();
        userList.add(sampleRole);
        given(service.getRoleList()).willReturn(userList);

        ResultMatcher hasRecord = jsonPath("$").isArray();
        MvcResult result = this.mockMvc.perform(get(PATH)
                .accept(MediaType.APPLICATION_JSON).header(CONTENT_TYPE,
                        MediaType.APPLICATION_JSON))
                .andExpect(OK).andExpect(RESPONSE_FORMAT)
                .andExpect(hasRecord).andReturn();

        String jsonString = result.getResponse().getContentAsString();
        DocumentContext context = JsonPath.parse(jsonString);
        int length = context.read("$.length()");
        assertThat(length, org.hamcrest.CoreMatchers.is(1));
    }


    @Test
    public void thatGetRoleWhenPassinRoleId() throws Exception {

        int SOME_ROLE_ID = 1;
        given(service.getRole(SOME_ROLE_ID)).willReturn(sampleRole);
        MvcResult result = this.mockMvc.perform(get(PATH + "/" + SOME_ROLE_ID)
                .accept(MediaType.APPLICATION_JSON)
                .header(CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(OK).andExpect(RESPONSE_FORMAT)
                .andReturn();
        String jsonString = result.getResponse().getContentAsString();
        Role role = new Gson().fromJson(jsonString, Role.class);
        assertThat(role, org.hamcrest.CoreMatchers.is(notNullValue()));
    }

    @Test
    public void thatCreateNewRoleWhenPassingNewRoleInformation() throws Exception {
        given(service.createRole(Mockito.any(Role.class))).willReturn(sampleRole);
        String content = new Gson().toJson(sampleRole);
        this.mockMvc.perform(post(PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(OK)
                .andExpect(jsonPath("$.name").value(SOME_ROLE_NAME))
                .andReturn();
    }

    @Test
    public void thatUpdateRoleWhenPassingRoleInfo() throws Exception {
        int SOME_ROLE_ID = 1;
        given(service.updateRole(Mockito.any(Role.class), Mockito.anyInt()))
                .willReturn(sampleRole);
        String content = new Gson().toJson(sampleRole);
        this.mockMvc.perform(put(PATH + "/" + SOME_ROLE_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(OK)
                .andExpect(jsonPath("$.name").value(SOME_ROLE_NAME))
                .andReturn();
    }


    @Test
    public void thatDeleteRoleWhenRequestDeleteRole() throws Exception {
        int SOME_ROLE_ID = 1;
        this.mockMvc.perform(delete(PATH + "/" + SOME_ROLE_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(OK).andReturn();
    }

}
