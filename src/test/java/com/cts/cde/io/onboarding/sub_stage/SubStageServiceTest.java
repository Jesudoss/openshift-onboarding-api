package com.cts.cde.io.onboarding.sub_stage;

import com.cts.cde.io.onboarding.content.Content;
import com.cts.cde.io.onboarding.stage.Stage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class SubStageServiceTest {


    @InjectMocks
    private SubStageService service;

    private String SOME_NAME = "somename";
    @Mock
    private SubStageRepository repository;

    private Set<Content> contentList = new HashSet() {
        {
            add(new Content());
        }
    };
    private SubStage sampleSubStage = new SubStage(1, SOME_NAME, new Stage(), contentList);
    private SubStageFilter sampleSubStageFilter = new SubStageFilter(1, SOME_NAME, new Stage());

    @Before
    public void setup() {
        sampleSubStage.setName(SOME_NAME);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void thatGetListOfSubStagesWhenRequestAllSubStages() {
        //Given
        List<SubStageFilter> mockData = new ArrayList<>();
        mockData.add(sampleSubStageFilter);
        when(repository.findAllWithFilter()).thenReturn(mockData);
        List<SubStageFilter> list = service.getSubStageList();
        assertEquals(1, list.size());
    }

    @Test
    public void thatGetParticularSubStageWhenRequestBySubstageId() {

        when(repository.findOne(1)).thenReturn(sampleSubStage);
        SubStage client = service.getSubStage(1);
        assertEquals(client.getName(), SOME_NAME);
    }


    @Test
    public void thatCreateSubStageWhenPassingNewSubStageInfo() {
        when(repository.save(sampleSubStage)).thenReturn(sampleSubStage);
        SubStage subStage = service.createSubStage(sampleSubStage);
        assertEquals(subStage.getName(), SOME_NAME);
    }

    @Test
    public void thatUpdateSubStageWhenPassingUpdatedSubStagesInfo() {
        when(repository.save(sampleSubStage)).thenReturn(sampleSubStage);
        service.updateSubStage(sampleSubStage, 1);
        assertTrue(true);
    }

    @Test
    public void thatDeleteRoleWhenPassingRoleId() {
        service.deleteSubStage(1);
        assertTrue(true);
    }


}