package com.cts.cde.io.onboarding.contentstatus;


import com.cts.cde.io.onboarding.content.Content;
import com.cts.cde.io.onboarding.usermanagement.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

public class ContentStatusServiceTest {

    @InjectMocks
    ContentStatusService contentStatusService;

    @Mock
    ContentStatusRepository contentStatusRepository;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void ThatGetListofContentStatusByUserId() throws Exception {
        List<ContentStatusFilter> dummyContentFilter= new ArrayList<>();
        dummyContentFilter.add(new ContentStatusFilter(1,1,1, ContentStatus.Status.COMPLETED));
        when(contentStatusRepository.findByUser(anyInt())).thenReturn(dummyContentFilter);
        List<ContentStatusFilter> contentStatusData = contentStatusService.getContentStatusByUserId(anyInt());
        assertEquals(1, contentStatusData.size());
    }

    @Test
    public void ThatGetListofContentStatusByUserIdWithEmptyContent() throws Exception {
        List<ContentStatusFilter> dummyContentFilter= new ArrayList<>();
        when(contentStatusRepository.findByUser(anyInt())).thenReturn(dummyContentFilter);
        List<ContentStatusFilter> contentStatusData = contentStatusService.getContentStatusByUserId(anyInt());
        assertEquals(0, contentStatusData.size());
    }

    @Test
    public void ThatGetListofContentStatusByUserIdForCompletedStatusCheck() throws Exception {
        List<ContentStatusFilter> dummyContentFilter= new ArrayList<>();
        dummyContentFilter.add(new ContentStatusFilter(1,1,1, ContentStatus.Status.COMPLETED));
        when(contentStatusRepository.findByUser(anyInt())).thenReturn(dummyContentFilter);
        List<ContentStatusFilter> contentStatusData = contentStatusService.getContentStatusByUserId(anyInt());
        assertEquals(ContentStatus.Status.COMPLETED, contentStatusData.get(0).getStatus());
    }

    @Test
    public void ThatGetListofContentStatusByUserIdForInprogressStatusCheck() throws Exception {
        List<ContentStatusFilter> dummyContentFilter= new ArrayList<>();
        dummyContentFilter.add(new ContentStatusFilter(1,1,1, ContentStatus.Status.INPROGRESS));
        when(contentStatusRepository.findByUser(anyInt())).thenReturn(dummyContentFilter);
        List<ContentStatusFilter> contentStatusData = contentStatusService.getContentStatusByUserId(anyInt());
        assertEquals(ContentStatus.Status.INPROGRESS, contentStatusData.get(0).getStatus());
    }




    @Test
    public void createUserContentStatus() throws Exception {
        ContentStatus dummyContentStatus = new ContentStatus(1,new User(),new Content(), ContentStatus.Status.INPROGRESS);
        when(contentStatusRepository.save(dummyContentStatus)).thenReturn(dummyContentStatus);
        ContentStatus returnedContentStatus = contentStatusService.createUserContentStatus(dummyContentStatus);
        assertEquals(1, returnedContentStatus.getId());



    }

}