package com.cts.cde.io.onboarding.sub_stage;

import com.cts.cde.io.onboarding.OnBoardingApplication;
import com.cts.cde.io.onboarding.role.Role;
import com.cts.cde.io.onboarding.content.Content;
import com.cts.cde.io.onboarding.stage.Stage;
import com.google.gson.Gson;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OnBoardingApplication.class)
@WebMvcTest(value = SubStageController.class, secure = false)
public class SubStageControllerTest {


    public static final String CONTENT_TYPE = "Content-type";
    public static final ResultMatcher OK = status().isOk();
    public static final ResultMatcher RESPONSE_FORMAT = content().contentType(MediaType.APPLICATION_JSON_UTF8);

    public static final String PATH = "/sub-stages";
    
//    @MockBean
//    private FlywayMigrationInitializer flywayMigrationInitializer;
    
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    SubStageController controller;

    @MockBean
    SubStageService service;


    private Set<Content> contentList = new HashSet() {
        {
            add(new Content());
        }
    };
    private String SOME_NAME = "some_name";
    private SubStage sampleSubStage = new SubStage(1, SOME_NAME, new Stage(), contentList);
    private SubStageFilter sampleSubStageFilter = new SubStageFilter(1, SOME_NAME, new Stage());

    @Before
    public void setUp() throws Exception {
        sampleSubStage.setId(1);
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.controller).build();
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void thatGetListOfRolesWhenFetchRoleList() throws Exception {

        List<SubStageFilter> userList = new ArrayList();
        userList.add(sampleSubStageFilter);
        given(service.getSubStageList()).willReturn(userList);

        ResultMatcher hasRecord = jsonPath("$").isArray();
        MvcResult result = this.mockMvc.perform(get(PATH)
                .accept(MediaType.APPLICATION_JSON).header(CONTENT_TYPE,
                        MediaType.APPLICATION_JSON))
                .andExpect(OK).andExpect(RESPONSE_FORMAT)
                .andExpect(hasRecord).andReturn();

        String jsonString = result.getResponse().getContentAsString();
        DocumentContext context = JsonPath.parse(jsonString);
        int length = context.read("$.length()");
        assertThat(length, is(1));
        assertThat(sampleSubStage.getStage(), is(notNullValue()));
    }


    @Test
    public void thatGetRoleWhenPassinRoleId() throws Exception {

        int SOME_ROLE_ID = 1;
        given(service.getSubStage(SOME_ROLE_ID)).willReturn(sampleSubStage);
        MvcResult result = this.mockMvc.perform(get(PATH + "/" + SOME_ROLE_ID)
                .accept(MediaType.APPLICATION_JSON)
                .header(CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(OK).andExpect(RESPONSE_FORMAT)
                .andReturn();
        String jsonString = result.getResponse().getContentAsString();
        Role role = new Gson().fromJson(jsonString, Role.class);
        assertThat(role, is(notNullValue()));
    }

    @Test
    public void thatCreateNewRoleWhenPassingNewRoleInformation() throws Exception {
        given(service.createSubStage(Mockito.any(SubStage.class))).willReturn(sampleSubStage);
        String content = new Gson().toJson(sampleSubStage);
        this.mockMvc.perform(post(PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(OK)
                .andExpect(jsonPath("$.name").value(SOME_NAME))
                .andReturn();
    }

    @Test
    public void thatUpdateRoleWhenPassingRoleInfo() throws Exception {
        int SOME_ROLE_ID = 1;

        String content = new Gson().toJson(sampleSubStage);
        this.mockMvc.perform(put(PATH + "/" + SOME_ROLE_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(OK)
                .andReturn();
    }


    @Test
    public void thatDeleteRoleWhenRequestDeleteRole() throws Exception {
        int SOME_ROLE_ID = 1;
        this.mockMvc.perform(delete(PATH + "/" + SOME_ROLE_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(OK).andReturn();
    }

}