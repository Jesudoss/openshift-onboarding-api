package com.cts.cde.io.onboarding.usermanagement;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.cts.cde.io.onboarding.OnBoardingApplication;
import com.cts.cde.io.onboarding.role.ErrorMessage;
import com.google.gson.Gson;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OnBoardingApplication.class)
@WebMvcTest(value = UserController.class, secure = false)
public class UserControllerTest {


    public static final String CONTENT_TYPE = "Content-type";
    public static final String SOME_USER = "some_user";

    public static final String USER = "/user";
    public static final ResultMatcher OK = status().isOk();
    public static final ResultMatcher RESPONSE_FORMAT = content().contentType(MediaType.APPLICATION_JSON_UTF8);
    private static final String SOMEPASSWORD = "somepassword";

    
//    @MockBean
//    private FlywayMigrationInitializer flywayMigrationInitializer;
    
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    UserController userController;

    @MockBean
    UserService userService;


    String expectedUserName = "someuser";
    User sampleUser = new User(expectedUserName,
            "someUserEmail" + System.currentTimeMillis(),
            SOMEPASSWORD,  "firstName",
            "lastName", "english",false);
    User sampleUserResponse = new User(expectedUserName,
            "someUserEmail" + System.currentTimeMillis(),
            SOMEPASSWORD, "firstName",
            "lastName", "english",false);


    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.userController).build();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void thatGettingSuccessSuserListtatusWhenFetchUserList() throws Exception {

        List<User> userList = new ArrayList();
        userList.add(sampleUser);
        given(userService.getUserList()).willReturn(userList);

        ResultMatcher hasRecord = jsonPath("$").isArray();
        MvcResult result = this.mockMvc.perform(get("/user")
                .accept(MediaType.APPLICATION_JSON).header(CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(OK).andExpect(RESPONSE_FORMAT)
                .andExpect(hasRecord).andReturn();

        String jsonString = result.getResponse().getContentAsString();
        DocumentContext context = JsonPath.parse(jsonString);
        int length = context.read("$.length()");
        System.out.println(length);
    }


    @Test
    public void thatGetSpecficUserDataWhenPassingRequestUserByUserName() throws Exception {

        given(userService.getUserByUserName(expectedUserName)).willReturn(sampleUser);

        MvcResult result = this.mockMvc.perform(get("/user/" + expectedUserName + "/")
                .accept(MediaType.APPLICATION_JSON)

                .header(CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(OK).andExpect(RESPONSE_FORMAT)
                .andReturn();
        String actualUserID = JsonPath.parse(result.getResponse()
                .getContentAsString()).read("$.userName");
        assertThat(actualUserID, is(expectedUserName));
    }


    @Test
    public void thatRegisterUserWhenCreatingUser() throws Exception {
        sampleUserResponse.setId(1);
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setStatus("Success");
        given(userService.createUser(Mockito.any(User.class))).willReturn(errorMessage);
        String content = new Gson().toJson(sampleUser);
        this.mockMvc.perform(post("/user/sign-up")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(OK)
                .andExpect(jsonPath("$.status").value("Success"))
                .andReturn();
    }

    //
//    @Test
//    public void thatUpdateUserInfoWhenPassingUserInfo() throws Exception {
//        sampleUser.setId(1);
//        given(userService.updateUser(Mockito.any(User.class), Mockito.anyInt())).willReturn(sampleUser);
//        ObjectMapper mapper = new ObjectMapper();
//        String content = mapper.writeValueAsString(sampleUser);
//        MvcResult result = this.mockMvc.perform(put("/user/1").content(content)
//                .accept(MediaType.APPLICATION_JSON)
//                .header(CONTENT_TYPE, MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.userName").value(expectedUserName))
//                .andReturn();
//    }

    @Test
    public void thatAuthenticateUserWhenPassingUserNameAndPlainPassword() throws Exception {
        sampleUserResponse.setId(1);
        given(userService.authenticateUser(Mockito.anyString(), Mockito.anyString())).willReturn(sampleUserResponse);
        String content = new Gson().toJson(sampleUser);
        this.mockMvc.perform(post("/user/authenticateUser")
                .contentType(MediaType.APPLICATION_JSON)
                .header("user_name", expectedUserName).header("password", SOMEPASSWORD))
                .andExpect(OK)
                .andExpect(jsonPath("$.userName").value(expectedUserName))
                .andReturn();
    }

    @Test
    public void thatAuthenticateUserWhenPassingInvalidUserNameAndPassword() throws Exception {
        sampleUserResponse.setId(1);
        given(userService.authenticateUser(Mockito.anyString(), Mockito.anyString())).willReturn(null);
        String content = new Gson().toJson(sampleUser);
        this.mockMvc.perform(post("/user/authenticateUser")
                .contentType(MediaType.APPLICATION_JSON)
                .header("user_name", "deyva").header("password", SOMEPASSWORD))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message").value("Invalid User Credentails."))
                .andReturn();
    }
    
}

