package com.cts.cde.io.onboarding.feedback;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.cts.cde.io.onboarding.usermanagement.User;
import org.springframework.boot.test.mock.mockito.MockBean;

public class FeedBackServiceTest {

//    @MockBean
//    private FlywayMigrationInitializer FlywayMigrationInitializer;
	
	@InjectMocks
    private FeedBackService service;

    @Mock
    private FeedBackRepository repository;
    
    User sampleUser = new User(1,
            "someUserEmail" + System.currentTimeMillis(),
            "password",  "firstName",
            "lastName", "english",false);
    
    FeedBack sampleFeedback= new FeedBack(sampleUser,"samplefeedback",new Date());
    List<FeedBack> expectedOutput = Arrays.asList(sampleFeedback);

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

	@Test
	public void testGetAllFeedbacks() {
		List<FeedBack> mockData = new ArrayList<>();
        mockData.add(sampleFeedback);
        when(repository.getAllistfeedbacks()).thenReturn(mockData);
        List<FeedBack> client = service.getAllistFeedbacks();
        assertEquals(1, client.size());
	}

	@Test
	public void testGetAllFeedbackList() {
		List<FeedBack> mockData = new ArrayList<>();
        mockData.add(sampleFeedback);
        when(repository.getAllfeedbacks()).thenReturn(mockData);
        List<FeedBack> Output = service.getAllFeedbackList();
        assertThat(Output, is(expectedOutput));
	}

	@Test
	public void testCreateUserFeedback() {
		when(repository.save(sampleFeedback)).thenReturn(sampleFeedback);
	}

}
