package com.cts.cde.io.onboarding.usermanagement;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class UserDetailsServiceImplTest {

    private UserDetailsServiceImpl service;

    @Mock
    private UserRepository repository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        service = new UserDetailsServiceImpl(repository);
    }


    @Test
    public void thatGettingExceptionWhenNoUserFoundInDatasource() {
        boolean exceptionCatch = false;
        String some_name = "some_name";
        when(repository.findByUserName(some_name)).thenReturn(null);
        try {
            service.loadUserByUsername(some_name);
        } catch (UsernameNotFoundException e) {
            exceptionCatch = true;
        }

        assertThat(exceptionCatch, is(true));

    }

}