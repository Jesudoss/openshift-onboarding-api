package com.cts.cde.io.onboarding;

import com.cts.cde.io.onboarding.contentstatus.ContentStatus;
import com.cts.cde.io.onboarding.stage.Stage;
import com.cts.cde.io.onboarding.sub_stage.SubStage;
import com.cts.cde.io.onboarding.usermanagement.User;
import com.cts.cde.io.onboarding.content.Content;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = OnBoardingApplication.class)
@TestPropertySource(locations = "classpath:application-test.yml")
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OnBoardingApplicationIntegrationTest {


    public static final String USER_SIGN_UP = "/user/sign-up";
    public static final String LOGIN = "/login";
    public static final String USER = "/user";
  //  public static final String ROLE = "/role";
  public static final String CONTENT_STATUS = "/content-status";
  
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String AUTHORIZATION = "Authorization";
    public static final String HTTP_LOCALHOST = "http://localhost:";
    public static final String USER_EMAILID = "wydnpipeline@cognizant.com";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

//    @MockBean
//    private FlywayMigrationInitializer flywayMigrationInitializer;

    Set<ContentStatus> contentStatuses = new HashSet(){
        {
            add(new ContentStatus(1, new User(), new Content(), ContentStatus.Status.COMPLETED));
        }
    };

    User user = new User(1, USER_EMAILID,
            "PASSWORD", "FIRST_NAME",
            "LAST_NAME", "ENGLISH",true);

    Stage dummyStage= new Stage();
    SubStage subStage = new SubStage(1,"CloudLift",dummyStage,null);

    Content dummyContent = new Content(1,"PCFCloudLift",subStage,1);

    ContentStatus dummyContentStatus = new ContentStatus(1,user,dummyContent,ContentStatus.Status.COMPLETED);
    MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
    private static String token;

    @Test
    public void that1RegisterUserWhenPassingUserInformation() {
        ResponseEntity<User> responseEntity =
                restTemplate.postForEntity(USER_SIGN_UP, user, User.class);
        User client = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void that2ReceiveTokenWhenRequestingTokenByPassingValidUserCredential() {
        ResponseEntity<User> responseEntity =
                restTemplate.postForEntity(LOGIN, user, User.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        token = responseEntity.getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
        assertTrue(responseEntity.getHeaders().containsKey(HttpHeaders.AUTHORIZATION));
    }

    @Test
    public void thatReceiveFailureStatusCodeWhenRequestingTokenByPassingInvalidUserCredential() {
        HttpStatus responseCode = null;
        user.setPassword("");
        ResponseEntity<User> userResponseEntity = restTemplate.postForEntity(LOGIN, user, User.class);
        if (userResponseEntity != null) {
            responseCode = userResponseEntity.getStatusCode();
        }
        assertThat(responseCode, is(HttpStatus.UNAUTHORIZED));
    }


    @Test
    public void thatGetAllUsersWhenRequestContainsValidToken() {
        ResponseEntity<Object> responseEntity = new TestRestTemplate().exchange(
                HTTP_LOCALHOST + port + USER, HttpMethod.GET,
                new HttpEntity<Object>(getHeader(token)),
                Object.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void thatGetForbiddenStatusWhenRequestContainsInvalidToken() {
        ResponseEntity<Object> responseEntity = new TestRestTemplate().exchange(
                HTTP_LOCALHOST + port + USER, HttpMethod.GET,
                new HttpEntity<Object>(getHeader("")),
                Object.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.FORBIDDEN));
    }

    @Test
    public void thatGetForbiddenStatusWhenRequestUserNameAndNpToken() {
        String url = HTTP_LOCALHOST + port + USER + "/" + USER_EMAILID;
        ResponseEntity<Object> responseEntity = new TestRestTemplate().exchange(
                url, HttpMethod.GET,
                new HttpEntity<Object>(getHeader("")),
                Object.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.FORBIDDEN));
    }

    @Test
    public void thatGetSpecificUserInfoWhenRequestContainsValidTokenAndUserName() {
        String url = HTTP_LOCALHOST + port + USER + "/" + USER_EMAILID + "/";
        ResponseEntity<Object> responseEntity = new TestRestTemplate().exchange(
                url, HttpMethod.GET,
                new HttpEntity<Object>(getHeader(token)),
                Object.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    }
//    @Ignore
//    @Test
//    public void thatGetUpdatedUserInfoWhenUpdateRequestContainsValidToken() {
//        User updateUser = new User();
//        updateUser.setFirstName("Demo");
//        updateUser.setLastName("Demo");
//        updateUser.setId(1);
//        Role role = new Role();
//        role.setId(1);
//        updateUser.setRole(role);
//
////        Role updateRole = new Role(1, "Architect", "Active", );
////
////        String urlForRoleCreate = HTTP_LOCALHOST + port + ROLE + "/1";
////        HttpEntity<Object> requestEntityForRoleCreate = new HttpEntity<>(updateRole, getHeader(token));
////        ResponseEntity<Object> responseEntity = new TestRestTemplate().exchange(
////                urlForRoleCreate, HttpMethod.PUT,
////                requestEntityForRoleCreate,
////                Object.class);
//        HttpEntity<Object> requestEntity = new HttpEntity<>(updateUser, getHeader(token));
//        String url = HTTP_LOCALHOST + port + USER + "/1";
//        ResponseEntity<Object> responseEntity = new TestRestTemplate().exchange(
//                url, HttpMethod.PUT,
//                requestEntity,
//                Object.class);
//        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
//    }

    @Test
    public void thatGetForbiddenStatusWhenUpdateRequestContainsInvalidToken() {
        HttpEntity<Object> requestEntity = new HttpEntity<>(user, getHeader(""));
        String url = HTTP_LOCALHOST + port + USER + "/1";
        ResponseEntity<Object> responseEntity = new TestRestTemplate().exchange(
                url, HttpMethod.PUT,
                requestEntity,
                Object.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.FORBIDDEN));
    }

    private MultiValueMap<String, String> getHeader(String token) {
        headers.add(CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
        headers.add(AUTHORIZATION, token);
        return headers;
    }

    @Test
    public void  thatGetContentStatusWhenUserIdValid(){
        String url = HTTP_LOCALHOST + port + CONTENT_STATUS + "/"+ user.getId();
        ResponseEntity<Object> responseEntity = new TestRestTemplate().exchange(
                url, HttpMethod.GET,
                new HttpEntity<Object>(getHeader(token)),
                Object.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    }
    @Test
    public void  thatGetContentStatusWhenUserIdInValid(){

        String url = HTTP_LOCALHOST + port + CONTENT_STATUS + "/"+"";
        ResponseEntity<Object> responseEntity = new TestRestTemplate().exchange(
                url, HttpMethod.GET,
                new HttpEntity<Object>(getHeader(token)),
                Object.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.METHOD_NOT_ALLOWED));
    }

}
