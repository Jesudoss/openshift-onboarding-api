package com.cts.cde.io.onboarding.RoleManagement;


import com.cts.cde.io.onboarding.content.Content;
import com.cts.cde.io.onboarding.content.Document;
import com.cts.cde.io.onboarding.role.ErrorMessage;
import com.cts.cde.io.onboarding.role.Role;
import com.cts.cde.io.onboarding.role.RoleRepository;
import com.cts.cde.io.onboarding.role.RoleService;
import com.cts.cde.io.onboarding.sub_stage.SubStage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class RoleServiceTest {

    @InjectMocks
    RoleService roleService;

    @Mock
    RoleRepository roleRepository;

    @Mock
    Content content;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    String expectedRoleName = "somerole";
    Role sampleRole = new Role(expectedRoleName,
            "Active",
            "role for someUser");
    String SOME_NAME = "My Content";
    List<Role> roleMockData = new ArrayList<>();

    @Test
    public void thatGetListOfRoleWhenRequestAllRoles() {

        roleMockData.add(sampleRole);
        when(roleRepository.findAll()).thenReturn(roleMockData);
        List<Role> client = roleService.getRoleList();
        assertEquals(1, client.size());
    }

    @Test
    public void thatGetParticularRoleWhenRequestByRoleId() {
        when(roleRepository.findOne(1)).thenReturn(sampleRole);
        Role client = roleService.getRole(1);
        assertEquals(client.getName(), expectedRoleName);
    }


    @Test
    public void thatCreateRoleWhenPassingNewRoleInfo() {
        when(roleRepository.save(sampleRole)).thenReturn(sampleRole);
        Role client = roleService.createRole(sampleRole);
        assertEquals(client.getName(), expectedRoleName);
    }

    @Test
    public void thatUpdateRoleWhenPassingUpdatedRoleInfo() {
        when(roleRepository.findOne(1)).thenReturn(sampleRole);
        when(roleRepository.save(sampleRole)).thenReturn(sampleRole);
        Role client = roleService.updateRole(sampleRole, 1);
        assertEquals(client.getName(), expectedRoleName);
    }


	//12/12/2017 - TDD for Delete operation Role without Content ,message display.
    @Test
    public void thatGettingErrorMessageWhenDeletingRoleWithoutContent() {
        // For Role not Associated with content
        ErrorMessage errorMessage = new ErrorMessage();
        int count = 0;
        when(roleRepository.deleteByRoleId(1)).thenReturn(count);
        if (count == 0) {
            errorMessage.setStatus("Success");
            errorMessage.setDescription("Successfully Deleted");
        }
        //WHEN
        ErrorMessage successMessage = roleService.deleteRole(1);
        String actualSuccessMessageStatus = successMessage.getStatus();
        String actualSuccessMessageDesc = successMessage.getDescription();

        //THEN
        assertEquals(errorMessage.getDescription(), successMessage.getDescription());
    }

	//12/12/2017 - TDD for Delete operation Role with Content ,message display.
    @Test
    public void thatGettingErrorMessageWhenDeletingRoleWithContent() {
        // For Role not Associated with content
        ErrorMessage erMessage = new ErrorMessage();

        int mockCount = 1;
        when(roleRepository.deleteByRoleId(1)).thenReturn(mockCount);
        if (mockCount > 0) {
            erMessage.setStatus("Failed");
            erMessage.setDescription("Content is associated with role");
        }

        //WHEN
        ErrorMessage succMessage = roleService.deleteRole(1);
        String actualSuccMessageStatus = succMessage.getStatus();
        String actualSuccMessageDesc = succMessage.getDescription();

        //THEN
        assertEquals(erMessage.getDescription(), actualSuccMessageDesc);

    }


}