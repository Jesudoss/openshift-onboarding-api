package com.cts.cde.io.onboarding.content;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.cts.cde.io.onboarding.OnBoardingApplication;
import com.cts.cde.io.onboarding.role.Role;
import com.cts.cde.io.onboarding.sub_stage.SubStage;
import com.google.gson.Gson;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OnBoardingApplication.class)
@WebMvcTest(value = ContentController.class, secure = false)
public class ContentControllerTest {

    public static final String CONTENT_TYPE = "Content-type";
    public static final ResultMatcher OK = status().isOk();
    public static final ResultMatcher RESPONSE_FORMAT = content().contentType(MediaType.APPLICATION_JSON_UTF8);

    public static final String PATH = "/contents";
    private static final String SOME_STAGE_NAME = "some_stage_name";

//    @MockBean
//    private FlywayMigrationInitializer flywayMigrationInitializer;
    
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ContentController controller;

    @MockBean
    ContentService service;

    private String SOME_NAME = "some_name";
    private Set<Role> roles = new HashSet() {
        {
            add(new Role(1, "ROLE_NAME", "ACTIVE", "DESC", null));
        }
    };
    List<Document> documents = new ArrayList() {{
        add(new Document(1, Document.Type.PDF, "", null));
    }};



    Content content = new Content(1, SOME_NAME, new SubStage()
            , roles, "TITLE", "HEADER",
            "DESC", "FOOTER",
            new Date(System.currentTimeMillis()), documents,1);

    List<Content> list = new ArrayList() {{
        add(content);
    }};


    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.controller).build();
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void thatGetListOfContentWhenRequestAllContent() throws Exception {

        List<ContentFilter> contentFilterList = new ArrayList();
        ContentFilter contentFilter = new ContentFilter();
        contentFilter.setId(1);
        contentFilter.setName(SOME_NAME);
        contentFilter.setStageName(SOME_STAGE_NAME);
        contentFilter.setSubStageName(SOME_STAGE_NAME);
        contentFilterList.add(contentFilter);

        given(service.getContentByRoleId(SOME_NAME)).willReturn(contentFilterList);

        ResultMatcher hasRecord = jsonPath("$").isArray();
        MvcResult result = this.mockMvc.perform(get(PATH + "?name=" + SOME_NAME)
                .accept(MediaType.APPLICATION_JSON).header(CONTENT_TYPE,
                        MediaType.APPLICATION_JSON))
                .andExpect(OK).andExpect(RESPONSE_FORMAT)
                .andExpect(hasRecord).andReturn();

        String jsonString = result.getResponse().getContentAsString();
        DocumentContext context = JsonPath.parse(jsonString);
        int length = context.read("$.length()");
        assertThat(length, is(1));
    }


    @Test
    public void thatGetContentModelWhenPassingContentId() throws Exception {

        int SOME_STAGE_ID = 1;
        given(service.getContent(SOME_STAGE_ID)).willReturn(content);
        MvcResult result = this.mockMvc.perform(get(PATH + "/" + SOME_STAGE_ID)
                .accept(MediaType.APPLICATION_JSON)
                .header(CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(OK).andExpect(RESPONSE_FORMAT)
                .andReturn();
        String jsonString = result.getResponse().getContentAsString();
        Content response = new Gson().fromJson(jsonString, Content.class);

        assertThat(response, is(notNullValue()));
    }


    @Test
    public void thatCreateNewContentWhenPassingNewContentInformation() throws Exception {
        given(service.createContent(Mockito.any(Content.class))).willReturn(content);
        String content = new Gson().toJson(getContent());
        this.mockMvc.perform(post(PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(OK)
                .andExpect(jsonPath("$.name").value(SOME_NAME))
                .andReturn();
    }

    @Test
    public void thatUpdateContentWhenPassingContentInfo() throws Exception {
        int SOME_ID = 1;
        given(service.updateContent(Mockito.any(Content.class)))
                .willReturn(content);
        Content content1 = getContent();
        assertThat(content1.toString(), is(notNullValue()));
        assertThat(content1.getSubStage(), is(notNullValue()));
        assertThat(content1.getDocuments().get(0).getContent(), is(notNullValue()));

        String content = new Gson().toJson(content1);
        this.mockMvc.perform(put(PATH + "/" + SOME_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(OK)
                .andExpect(jsonPath("$.name").value(SOME_NAME))
                .andReturn();
    }

    private Content getContent() {
        List<Document> documents = new ArrayList();
        Document document = new Document();
        document.setId(1);
        document.setType(Document.Type.PDF);
        document.setURL("URL");
        document.setContent(new Content());
        documents.add(document);

        Content src = new Content(1, SOME_NAME, null,1);
        src.setName(SOME_NAME);
        src.setRoles(roles);
        src.setDescription("");
        src.setTitle("title");
        src.setHeader("header");
        src.setFooter("footer");
        src.setDisplayOrder(1);
        src.setSubStage(new SubStage());
        src.setDocuments(documents);
        return src;
    }


    @Test
    public void thatDeleteContentWhenRequestDeleteContent() throws Exception {
        int SOME_ID = 1;
        this.mockMvc.perform(delete(PATH + "/" + SOME_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(OK).andReturn();
    }

}